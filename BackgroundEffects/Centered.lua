local Color1 = color(Var "Color1");

local t = Def.ActorFrame {
	LoadActor(Var "File1") .. {
		OnCommand= function(self)
			if GetUserPref("UserPrefBGAMode") == false then
				self:Load(nil);
			else
				self:xy(_screen.cx, _screen.cy):diffuse(Color1):effectclock("music")
				-- Explanation in StretchNoLoop.lua.
				if self.GetTexture then
					self:GetTexture():rate(self:GetParent():GetUpdateRate())
				end
			end
		end,
		BgaModeCommand=function(self)
			if GetUserPref("UserPrefBGAMode") == "false" then self:Load(nil); end;
		end;
		GainFocusCommand=cmd(playcommand,"BgaMode";play);
		LoseFocusCommand=cmd(playcommand,"BgaMode";pause);
	};
};

return t;
