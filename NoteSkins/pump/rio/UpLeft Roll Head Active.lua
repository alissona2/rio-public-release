local coord = {};


-- coord[#coord+1] =
-- {
-- 	["x"] = 0;
-- 	["y"] = 0;
-- }

coord[#coord+1] =
{
	["x"] = -16;
	["y"] = -16;
}

coord[#coord+1] =
{
	["x"] = 16;
	["y"] = -16;
}

-- coord[#coord+1] =
-- {
-- 	["x"] = 0;
-- 	["y"] = 0;
-- }

coord[#coord+1] =
{
	["x"] = -16;
	["y"] = 16;
}

coord[#coord+1] =
{
	["x"] = 16;
	["y"] = 16;
}

local t = Def.ActorFrame{};

t[#t+1] = NOTESKIN:LoadActor(Var "Button", "Receptor");

t[#t+1] = NOTESKIN:LoadActor( Var "Button", "Tap Note" ) .. {
	InitCommand=cmd(zoom,.75;playcommand, "Animate");
	AnimateCommand=function(self)
		local aux = ( self:getaux() >= 1  and self:getaux() <= #coord ) and self:getaux() or 1;

		self:x(coord[aux].x);
		self:y(coord[aux].y);

		(cmd(
			rotationz,math.random(-45,45);
			sleep,.5;
			queuecommand,"Animate";
		))(self);

		aux = aux == #coord and 1 or aux+1;
		self:aux( aux );
	end
};


return t;